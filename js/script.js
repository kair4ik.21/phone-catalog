
function editContact(id)
{
    console.log(id);
    $("#editContact").show();
    $("#saveContact").hide();

    $.ajax({
        type: 'POST',
        url: 'server.php',
        data: { phone_id: id },
        dataType:'json',
        beforeSend:function(xhr, settings){
            $(".jumbotron").show();
            $(".jumbotron p").text("Редактирование телефона");
        },
        success:function(data){
            console.log(data);

            if (data.phone !== undefined) {
                $("input[name=id]").val(data.id);
                $("input[name=firstName]").val(data.first_name);
                $("input[name=lastName]").val(data.last_name);
                $("input[name=phone]").val(data.phone);
            } else {
                alert("Заполните все поля");
            }
        },
        error: function(data) {
            // if error occured
        }
    });
}

function deleteContact(id)
{
    $.ajax({
        type: 'POST',
        url: 'server.php',
        data: { idForDelete: id },
        dataType:'json',
        beforeSend:function(xhr, settings){
        },
        success:function(data){
            console.log(data);

            if (data === "Успешно удален") {
                // alert("Успешно удален")
                $( "#allPhones div#"+id ).detach();
            }
        },
        error: function(data) {
            // if error occured
        }
    });

}

function contactTemplate(data)
{
   return "<div id="+data.id+">"+data.first_name + " " + data.last_name + " " + data.phone +"  <button onclick=\"editContact("+data.id+");\" class=\"btn btn-primary\">Редактировать</button> <button onclick=\"deleteContact("+data.id+");\" class=\"btn btn-danger\">Удалить</button> <hr></div>";
}


$(document).ready(function() {

    $("#addContact").click(function (e) {
            e.preventDefault();
            $(".jumbotron").show();
            $("#editContact").hide();
            $("#saveContact").show();
            $(".jumbotron p").text("Добавление нового телефона");
            $("input[type=text]").val("");
        }
    );

    $("#cancel").click(function (e) {
            e.preventDefault();
            $(".jumbotron").hide();
        }
    );

    $("#saveContact").click(function (e) {
            e.preventDefault();

            $.ajax({
                type: 'POST',
                url: 'server.php',
                data:$("#newContact").serialize(),
                dataType:'json',
                beforeSend:function(xhr, settings){

                },
                success:function(data){
                    console.log(data);

                    if (data.phone !== undefined) {
                        $(".jumbotron").hide();
                        console.log("Новый телефон успешно добавлен");
                        $("input[type=text]").val("");
                        $( "#allPhones" ).append(contactTemplate(data));
                        $( "#allPhones div#"+data.id ).animate({
                            backgroundColor: "#87CEFA",
                        }, 100);
                        $( "#allPhones div#"+data.id).animate({
                            backgroundColor: "white"
                        }, 1500 );
                    } else {
                        alert("Заполните все поля");
                    }

                    //  json response
                },
                error: function(data) {
                    // if error occured
                }
            });
        });

    $("#editContact").click(function (e) {
            e.preventDefault();

            $.ajax({
                type: 'POST',
                url: 'serverEdit.php',
                data:$("#newContact").serialize(),
                dataType:'json',
                beforeSend:function(xhr, settings){

                },
                success:function(data){
                    console.log(data);

                    if (data.phone !== undefined) {
                        $(".jumbotron").hide();
                        console.log("Контакт успешно изменен");
                        $("input[type=text]").val("");
                        $( "#allPhones div#"+data.id ).html( contactTemplate(data) );
                        $( "#allPhones div#"+data.id ).animate({
                            backgroundColor: "#87CEFA",
                        }, 100);
                        $( "#allPhones div#"+data.id).animate({
                            backgroundColor: "white"
                        }, 1500 );
                        console.log("И покрашен");
                    } else {
                        alert("Заполните все поля");
                    }

                    //  json response
                },
                error: function(data) {
                    // if error occured
                }
            });
        }
    );


    $.ajax({
        type: "POST",
        url: 'server.php',
        data: 'phone=all',
        success: function(response)
        {
            var phones = JSON.parse(response);

            $.each(phones, function (key, value) {
                $( "#allPhones" ).append( contactTemplate(value));
            });

        }
    });


    $("#random").click(function (e) {
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: 'server.php',
                data: 'type=random',
                success: function(response)
                {
                    var data = JSON.parse(response);

                    console.log(data);
                    $("input[name=firstName]").val(data.first_name);
                    $("input[name=lastName]").val(data.last_name);
                    $("input[name=phone]").val(data.phone);

                }
            });
        }
    );

});