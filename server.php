<?php
require_once 'vendor/autoload.php';
require_once 'config/Access.php';
require_once 'config/Database.php';
require_once 'models/PhoneCatalog.php';
require_once 'models/Security.php';

use app\models\PhoneCatalog;
use app\models\Security;


function getPhones()
{
    return $phones = PhoneCatalog::getAll();
}

function getPhone($id)
{
    return $phone = PhoneCatalog::getPhoneBy($id);
}

function deletePhone($id)
{
    $phone = PhoneCatalog::deletePhone($id);
    if (is_array($phone)) {
        return "Успешно удален";
    }

}

function test()
{
    $faker = Faker\Factory::create('ru_RU');
    echo $faker->firstName." ".$faker->lastName." ".$faker->phoneNumber ;
}

//test();


if (isset($_POST)) {

    if (isset($_POST['phone_id'])) {

        echo json_encode(getPhone($_POST['phone_id']));
    }

    if (isset($_POST['idForDelete'])) {

        echo json_encode(deletePhone($_POST['idForDelete']));
    }

    if (isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['phone']) && !isset($_POST['phone_id'])) {
        $firstName = Security::clearData($_POST['firstName']);
        $lastName = Security::clearData($_POST['lastName']);
        $phone = Security::clearData($_POST['phone']);

        $newContact = new PhoneCatalog();
        $newContact->firstName = $firstName;
        $newContact->lastName = $lastName;
        $newContact->phone = $phone;
        $result = $newContact->addContact();

        echo json_encode($result);
    }

    if (isset($_POST['phone']) && $_POST['phone'] == 'all') {
        echo json_encode(getPhones());
    }

    if (isset($_POST['type']) && $_POST['type'] == 'random') {
        $faker = Faker\Factory::create('ru_RU');

        $data = [
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'phone' => $faker->phoneNumber,
        ];
        echo json_encode($data);
    }


} else {
    echo json_encode(array('success' => 0));
}