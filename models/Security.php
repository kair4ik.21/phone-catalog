<?php


namespace app\models;


class Security
{

    public static function clearData($string)
    {
        $specialSymbols = ["<", ">", "\'", "\"", "*", "_", "?", "!", "\\", "|", "/", ".", ","];
        $result = str_replace($specialSymbols, "", $string);
        return $result;
    }

}