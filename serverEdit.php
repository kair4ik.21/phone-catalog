<?php
require_once 'config/Access.php';
require_once 'config/Database.php';
require_once 'models/PhoneCatalog.php';
require_once 'models/Security.php';

use app\models\PhoneCatalog;
use app\models\Security;

function getPhone($id)
{
    return $phone = PhoneCatalog::getPhoneBy($id);
}

if (isset($_POST)) {

    if (isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['phone']) && isset($_POST['id'])) {
        $firstName = Security::clearData($_POST['firstName']);
        $lastName = Security::clearData($_POST['lastName']);
        $phone = Security::clearData($_POST['phone']);
        $id = Security::clearData($_POST['id']);

        $newContact = new PhoneCatalog();
        $newContact->firstName = $firstName;
        $newContact->lastName = $lastName;
        $newContact->phone = $phone;
        $result = $newContact->editContact($id);

        echo json_encode($result);
    }

}
