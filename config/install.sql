
CREATE TABLE public.phone (
                              id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
                              first_name varchar NULL,
                              last_name varchar NULL,
                              phone varchar NULL,
                              CONSTRAINT phone_pk PRIMARY KEY (id)
);
