<?php


namespace app\models;

use app\config\Database;

class PhoneCatalog
{

    public $firstName;
    public $lastName;
    public $phone;
    private static $table = 'phone';

    public static function getAll() {
        $db = new Database();
        $result = $db->queryAll("SELECT * from ".self::$table." order by id asc");
        return $result;
    }

    public static function getPhoneBy($id) {
        $db = new Database();
        $result = $db->queryOne("SELECT * from ".self::$table. " WHERE id = ".$id);
        return $result;
    }


    public static function getId() {
        $db = new Database();
        $result = $db->queryOne("SELECT MAX(ID) from ".self::$table);
        return $result;
    }


    public function addContact()
    {
        $data = [
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'phone' => $this->phone,
        ];

        if ($this->firstName != '' && $this->lastName != '' && $this->phone != '') {
            $db = new Database();
            $result = $db->querySave('INSERT INTO '.self::$table.' (first_name , last_name , phone) VALUES (:first_name, :last_name, :phone)', $data);
            if ($result) {

                $id = self::getId();
                $data['id'] = $id['max'];
                return $data;
            } else {
                return $result;
            }
        } else {
            return "Заполните все поля";
        }
    }

    public function editContact($id)
    {
        $data = [
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'phone' => $this->phone,
            'id' => $id,
        ];

        if ($this->firstName != '' && $this->lastName != '' && $this->phone != ''&& $id != '') {
            $db = new Database();
            $result = $db->queryOne('UPDATE '.self::$table.' SET first_name = \''.$this->firstName.'\' , last_name = \''.$this->lastName.'\' , phone = \''.$this->phone.'\' WHERE id='.$id);
            if (is_array($result)) {
                return $data;
            } else {
                return $result;
            }
        } else {
            return "Заполните все поля";
        }
    }


    public static function deletePhone($id) {
        $db = new Database();
        $result = $db->queryOne("DELETE from ".self::$table. " WHERE id = ".$id);
        return $result;
    }

    public static function test() {
        return 5;
    }

}