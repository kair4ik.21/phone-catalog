<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="node_modules/bootstrap3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="node_modules/jquery/dist/jquery.min.js"> </script>
    <script src="node_modules/bootstrap3/dist/js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet">
    <script src="js/script.js"></script>
    <script src="node_modules/jquery-ui/ui/effect.js"></script>
</head>

<body>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">Телефонный справочник</div>
        <div class="panel-body">
            <button id="addContact" class="btn btn-primary">Добавить</button>
            <hr>
            <div id="allPhones">

            </div>
        </div>
    </div>

    <div class="jumbotron">
        <p>Добавление нового телефона</p>
        <form action="#" id="newContact">
            <input type="hidden" name="id">
            <input class="form-control" type="text" name="firstName" placeholder="Имя" required>
            <input class="form-control" type="text" name="lastName" placeholder="Фамилия" required>
            <input class="form-control" type="text" name="phone" placeholder="Телефон" required>
            <br>
            <button id="random" class="btn btn-primary">Рандомные данные</button>
            <input id="saveContact" class="btn btn-success" type="submit" value="Сохранить">
            <input id="editContact" class="btn btn-success" type="submit" value="Редактировать">
            <button id="cancel" class="btn btn-default">Отмена</button>
        </form>
    </div>
</div>
</html>

