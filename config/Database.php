<?php

namespace app\config;

use PDO;
use PDOException;

class Database
{

    const USERNAME = Access::USERNAME;
    const PASSWORD = Access::PASSWORD;

    private $dns;

    /**
     * @return mixed
     */
    private function getDns()
    {
        return "pgsql:host=".Access::HOST.";port=5432;dbname=".Access::DB_NAME;
    }


    public function queryOne($query)
    {
        try {
            $pdo = new PDO($this->getDns(), self::USERNAME, self::PASSWORD);
        } catch (PDOException $e) {
            return $e->getMessage();
        }

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function querySave($query, $data)
    {
        try {
            $pdo = new PDO($this->getDns(), self::USERNAME, self::PASSWORD);
        } catch (PDOException $e) {
            return $e->getMessage();
        }
        $result = $pdo->prepare($query)->execute($data);
        return $result;
    }

    public function queryAll($query)
    {
        try {
            $pdo = new PDO($this->getDns(), self::USERNAME, self::PASSWORD);
        } catch (PDOException $e) {
            return $e->getMessage();
        }
        $stmt = $pdo->query($query);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }


}