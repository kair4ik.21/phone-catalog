<?php


namespace app\config;


class AccessExample
{
    const USERNAME = 'postgres';
    const PASSWORD = 'postgres';
    const HOST = 'localhost';
    const DB_NAME = 'task1';
}